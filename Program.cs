﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

using FMOD;

namespace SoundGenerator
{
    class Program
    {
        static float t1 = 0, t2 = 0;

        static unsafe FMOD.RESULT PCMREADCALLBACK( IntPtr soundraw, IntPtr data, uint datalen )
        {
            //
            // FMOD haluaa että raaka äänidata kirjoitetaan muistipaikkaan johon soundraw osoittaa.
            // Vähä häröö käyttää raakoja pointtereita näin C# ympäristössä mut ei muita mahdollisuuksia tän kirjaston kans.
            // Methodin määritksessä on 'unsafe' että voidaan tehdä näin ja lisäksi täytyy projektin asetuksista erikseen sallia 'unsafe' käyttö.
            // 
            var stereo16bitbuffer = (short*)data.ToPointer();

            // datalen on tavuina se samplejen määrä jonka FMOD haluaa
            // ja meillähän on 4 tavua per sample koska 16-bittinen formaatti ja kaksi kanavaa joten jaetaan datalen neljällä.
            // (datalen >> 2) jakaa siis neljällä bitshift operaationa, tyylittelyä nyt kun käytetään raakoja pointtereitakin.
            for ( var count = 0; count < (datalen >> 2); count++ )
            {
                // Lasketaan kanaville arvot. Math.Sin tietenkin tässä luo perus sin aaltoa ja 
                // * 32767.0f skaalaa sen 16-bittisen PCM datan tasolle eli 0-32767
                var vasenKanava = (short)(Math.Sin( t1 ) * 32767.0f);
                var oikeaKavana = (short)(Math.Sin( t2 ) * 32767.0f);

                // Ajetaan 'aikaa' eteenpäin.
                t1 += 0.1f;
                t2 += 0.1f;

                //
                // *stereo16bitbuffer++ on taas pointteri operaatioita ja tää tarkottaa et 
                // ensin niin sanotusti 'de-referencetaan' pointteri että sen arvoa voidaan käyttää ja
                // asetetaan siihen arvo jonka jälkeen siirretään pointteri yhden short:in verran eteenpäin.
                //
                *stereo16bitbuffer++ = vasenKanava;
                *stereo16bitbuffer++ = oikeaKavana;
            }

            return FMOD.RESULT.OK;
        }

        static void FMODErrorCheck( FMOD.RESULT result )
        {
            if ( result != FMOD.RESULT.OK )
            {
                Console.WriteLine( "FMOD error! " + result + " - " + FMOD.Error.String( result ) );
                Console.ReadLine();

                Environment.Exit( -1 );
            }
        }

        static void Main( string[] args )
        {
            FMOD.System system = null;
            FMOD.Sound sound = null;
            FMOD.Channel channel = null;

            // Luodaan äänijärjestelmä.
            FMODErrorCheck( FMOD.Factory.System_Create( ref system ) );
            // Alustetaan äänijärjestelmä.
            FMODErrorCheck( system.init( 32, FMOD.INITFLAGS.NORMAL, (IntPtr)null ) );

            //
            // Äänen tiedot.
            //
            var channels = 2;
            var frequency = 44100;
            var soundInfo = new FMOD.CREATESOUNDEXINFO();
            soundInfo.cbsize = Marshal.SizeOf( soundInfo );
            soundInfo.fileoffset = 0;
            soundInfo.format = FMOD.SOUND_FORMAT.PCM16;
            soundInfo.numchannels = channels;
            soundInfo.defaultfrequency = frequency;
            //
            // Äänen pituus tavuina.
            // Eli sampleja on sekunnissa taajuus * kanavat
            // ja äänen tyyppi on 16-bittinen eli kaksitavuinen niin kerrotaan se kahdella 
            // ja luodaan kaksi sekuntia dataa joten kerrotaan taas kahdella.
            //
            soundInfo.length = (uint)frequency * (uint)channels * 2 * 2;
            //
            // Asetetaan kirjastolle methodi mitä se kutsuu kun tarvitsee raakaa ääni dataa.
            // 
            soundInfo.pcmreadcallback = PCMREADCALLBACK;

            // Äänen mode.
            var mode = (FMOD.MODE._2D |          /* Ei käytetä kirjaston 3D ominaisuuksia. */
                        FMOD.MODE.OPENUSER |     /* Käyttäjän luoma ääni. */
                        FMOD.MODE.LOOP_NORMAL |  /* Looppaus päälle. */
                        FMOD.MODE.HARDWARE |     /* Laitteisto kiihdytys. */
                        FMOD.MODE.CREATESTREAM); /* Stream-tyyppinen ääni. */

            // Luodaan ääni.
            FMODErrorCheck( system.createSound(
                (string)null,
                mode,
                ref soundInfo,
                ref sound ) );

            // Ääni soimaan.
            FMODErrorCheck( system.playSound( FMOD.CHANNELINDEX.FREE, sound, false, ref channel ) );

            // Volume on välillä 0.0 - 1.0, laitetaan hiljemmalle.
            FMODErrorCheck( channel.setVolume( 0.01f ) );

            while ( true )
            {
                //
                // Haetaan äänen toisto kohta ja pituus ja printataan.
                //
                var ms = 0u;
                var lenms = 0u;

                channel.getPosition( ref ms, FMOD.TIMEUNIT.MS );
                sound.getLength( ref lenms, FMOD.TIMEUNIT.MS );

                Console.WriteLine( "{0}ms / {1}ms", ms, lenms );

                // Päivitetään äänikirjasto.
                system.update();
            }
        }
    }
}
